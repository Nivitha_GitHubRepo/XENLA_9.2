# Xenopus laevis 9.2 genome assembly files

This project houses gene models (GFF) and other metadata files for the X. laevis 9.2 genome assembly.

## File List

XENLA_9.2_Xenbase.gff - Gene models for X. laevis 9.2 genome assembly.

### Branches

Master - This branch holds the stable releases of the GFF file.

Name_metadata - This branch holds incremental changes to model names, dbxrefs, etc.

Structural - This branch holds incremental changes to gene model structures, including: adding, removing, and altering exons and transcripts, as well as adding/removing gene models.

### External Files

[Xenopus laevis 9.2 genome FASTA file](ftp://ftp.xenbase.org/pub/Genomics/JGI/Xenla9.2/XL9_2.fa.gz)



